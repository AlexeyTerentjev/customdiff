@ECHO OFF
setlocal EnableExtensions EnableDelayedExpansion

REM CHECK/CHANGE THESE TWO VARIABLES BELOW BEFORE USING SCRIPT!
set TORTOISE_SCRIPTS_PATH=C:\Program Files\TortoiseSVN\Diff-Scripts\
set TORTOISE_MERGE_PATH=%TORTOISE_SCRIPTS_PATH%..\bin\TortoiseMerge.exe 

set base=%~f1
set mine=%~f2

FOR %%i IN ("%base%") DO (
set FILE_EXT=%%~xi
)

set _tempvar=0
REM Word files
IF /I "%FILE_EXT%"==".doc"  set _tempvar=1 
IF /I "%FILE_EXT%"==".docx" set _tempvar=1
IF /I "%FILE_EXT%"==".docm" set _tempvar=1
IF %_tempvar% EQU 1 start /WAIT "" "wscript.exe" "%TORTOISE_SCRIPTS_PATH%diff-doc.js" "%base%" "%mine%" //E:javascript && exit

REM EXE/DLL files
IF /I "%FILE_EXT%"==".dll"  set _tempvar=1 
IF /I "%FILE_EXT%"==".exe"  set _tempvar=1
IF %_tempvar% EQU 1 wscript.exe "%TORTOISE_SCRIPTS_PATH%diff-dll.vbs" "%base%" "%mine%" //E:vbscript && exit

REM NB files
IF /I "%FILE_EXT%"==".nb"   set _tempvar=1
IF %_tempvar% EQU 1 wscript.exe "%TORTOISE_SCRIPTS_PATH%diff-nb.vbs" "%base%" "%mine%" //E:vbscript && exit

REM ODT files
IF /I "%FILE_EXT%"==".ods"  set _tempvar=1 
IF /I "%FILE_EXT%"==".odt"  set _tempvar=1
IF %_tempvar% EQU 1 wscript.exe "%TORTOISE_SCRIPTS_PATH%diff-odt.vbs" "%base%" "%mine%" //E:vbscript && exit


REM PPT files
IF /I "%FILE_EXT%"==".ppt"  set _tempvar=1 
IF /I "%FILE_EXT%"==".pptm" set _tempvar=1
IF /I "%FILE_EXT%"==".pptx" set _tempvar=1
if %_tempvar% EQU 1 wscript.exe "%TORTOISE_SCRIPTS_PATH%diff-ppt.js" "%base%" "%mine%" //E:javascript && exit

REM SXW files
IF /I "%FILE_EXT%"==".sxw"  set _tempvar=1 
IF %_tempvar% EQU 1 wscript.exe "%TORTOISE_SCRIPTS_PATH%diff-sxw.vbs" "%base%" "%mine%" //E:vbscript && exit

REM EXCEL files
IF /I "%FILE_EXT%"==".xlam" set _tempvar=1 
IF /I "%FILE_EXT%"==".xls"  set _tempvar=1
IF /I "%FILE_EXT%"==".xlsb" set _tempvar=1
IF /I "%FILE_EXT%"==".xlsm" set _tempvar=1
IF /I "%FILE_EXT%"==".xlsx" set _tempvar=1
IF %_tempvar% EQU 1 wscript.exe "%TORTOISE_SCRIPTS_PATH%diff-xls.js" "%base%" "%mine%" //E:javascript && exit

REM Finally, fallback to conventional diff
"%TORTOISE_MERGE_PATH%" %mine% %base%
