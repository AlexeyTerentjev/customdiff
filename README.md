﻿# Custom diff script

# Description

This batch script is a handy tool that utilizes custom diff tools from 
Tortoise SVN for SourceTree.

# Prerequisites 

Prior to diving into marvelous universe of custom diffs, one should have:

1. Microsoft Windows system installed.
2. TortoiseSVN (or TortoiseGit, TortoiseHg) installed on that system.
3. SourceTree.

# Installation

1. Put batch file in a safe place.
2. Open it.
3. Ensure that variables `TORTOISE_SCRIPTS_PATH` and `TORTOISE_MERGE_PATH` point
   to corresponding places on your PC. First should contain script files, e.g. `diff-doc.js`.
   The second variable should contain path to Tortoise merge executable. Sample file 
   contains values for default installation of TortoiseSVN.
4. Save changes if any.
5. Copy path to `.bat` file.
6. Open SourceTree. 
7. Navigate to `Tools -> Options`.
8. Open `Diff` tab. Change value of `External Diff Tool` to `Custom`.
9. Type path to your batch file into `Diff Command`, for instance, 
   `C:\Programs\CustomDiff\CustomDiff.bat`.
10. Type `\"$LOCAL\" \"$REMOTE\" ` into `Arguments` to the right of `Diff Command`.
11. Try viewing diffs in current changes `RMB -> External Diff` or in history 
    `Log Selected -> RMB Any version -> External Diff`. If something is not working,
    contact me.

宝

